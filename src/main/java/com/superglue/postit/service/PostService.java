package com.superglue.postit.service;

import com.superglue.postit.model.Post;
import com.superglue.postit.repository.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class PostService {
  @Autowired PostRepository postRepository;

  public List<Post> retrieveAllPosts() {
    List<Post> sortedPosts =
        StreamSupport.stream(postRepository.findAll().spliterator(), false)
            .sorted(Comparator.comparing(Post::getCreatedAt))
            .collect(Collectors.toList());
    return sortedPosts;
  }

  public Post createPost(Post post) {
    return postRepository.save(post);
  }
}
