package com.superglue.postit.controller;

import com.superglue.postit.model.Post;
import com.superglue.postit.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping
public class PostController {

  @Autowired private PostService postService;

  @GetMapping
  public String getAllPosts(Model model) {
    List<Post> posts = postService.retrieveAllPosts();
    model.addAttribute("posts", posts);
    model.addAttribute("post", new Post());
    return "index";
  }

  @PostMapping
  public String sendPost(
      @Valid @ModelAttribute("post") Post post, BindingResult result, Model model) {
    //  model.addAttribute("post", new Post());
    if (result.hasErrors()) {
      List<Post> posts = postService.retrieveAllPosts();
      model.addAttribute("posts", posts);
      return "index";
    }
    // model.addAttribute("post", postMessage);
    postService.createPost(post);
    // model.addAttribute("posts", postService.retrieveAllPosts());
    return "redirect:/";
  }
}
